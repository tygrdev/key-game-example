;;; key-game-example.el --- Example key-based tutorial games -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 0.0.1
;; File: key-game-example.el
;; Keywords: games
;; URL: https://gitlab.com/grinn.amy/key-game-example
;; Package-Requires: ((emacs "25.1") (key-game "0.0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; An example tutorial game based on key-game.el

;;; Code:

;;;; Requirements

(require 'key-game)

;;;###autoload (autoload 'key-game-example "key-game-example.el" "An example key-based tutorial game.")
(key-game key-game-example
  "An example key-based tutorial game."
  :title "Example Key Game"
  :message "Welcome to the example game!"
  key-game-example-level-1
  key-game-example-level-2)

;;;; Level 1

(key-game-level key-game-example-level-1
  "Level 1: Intro."
  :intro key-game-example-level-1-intro
  :next key-game-example-level-2)

(key-game-frame key-game-example-level-1-intro
  :read-only t
  (insert "\n\nThis is an example game created with key-game.el."))

;;;; Level 2

(key-game-level key-game-example-level-2
  "Level 2: Challenges."
  :intro key-game-example-level-2-intro
  key-game-example-level-2-frame-1
  key-game-example-level-2-frame-2)

(key-game-frame key-game-example-level-2-intro
  :read-only t
  (insert "

Here are two example frames which use keys and commands to
validate frames."))

(key-game-frame key-game-example-level-2-frame-1
  :read-only t
  :commands (backward-word previous-line)
  (insert "\n\nMove backward one word and\nthen up one line."))

(key-game-frame key-game-example-level-2-frame-2
  :keys ("h" "e" "l" "l" "o")
  (insert "\n\nType the word \"hello\": ")
  (open-line 1))


(provide 'key-game-example)

;;; key-game-example.el ends here
